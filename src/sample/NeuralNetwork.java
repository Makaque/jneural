package sample;

import com.sun.xml.internal.ws.util.StreamUtils;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NeuralNetwork {
    List<Integer> layer_sizes;
    List<Pair<Integer, Integer>> weight_shapes;
    List<PMatrix> weights;
    List<PMatrix> biases;


    public NeuralNetwork(Integer... layer_sizes) {
        Random r = new Random();

        this.layer_sizes = Arrays.asList(layer_sizes);
        weight_shapes = IntStream.range(0, layer_sizes.length)
                .parallel()
                .mapToObj(i -> new Pair<Integer, Integer>(i, layer_sizes[i]))
                .skip(1)
                .map(ls -> new Pair<Integer, Integer>(ls.getValue(), layer_sizes[ls.getKey() - 1]))
                .collect(Collectors.toList());

        weights = weight_shapes.parallelStream()
                .map(ws -> new PMatrix(ws.getKey(), ws.getValue()))
                .map(m -> m.map(val -> r.nextGaussian()/Math.sqrt(m.numCols)))
                .collect(Collectors.toList());

        biases = this.layer_sizes.parallelStream()
                .skip(1)
                .map(s -> new PMatrix(s, 1))
                .collect(Collectors.toList());

//        System.out.println(this.layer_sizes);
//        System.out.println(weight_shapes);
//        System.out.println(weights);
//        System.out.println(biases);
    }

    public PMatrix predict(PMatrix input) {
        Box<PMatrix> predictor = new Box<>(input);
        IntStream.range(0, weights.size())
                .mapToObj(i -> new Pair<PMatrix,PMatrix>(weights.get(i), biases.get(i)))
                // P = activation(W * P + B)
                .forEach(WB -> predictor.set(activation(PMatrix.addColVec(PMatrix.multiply(WB.getKey(), predictor.get()), WB.getValue()))));
        return predictor.get();
    }

    public static PMatrix activation(PMatrix X){
        return X.map(NeuralNetwork::activation);
    }

    public static double activation(double x) {
        return 1 / (1 + Math.exp(-x));
    }
}
