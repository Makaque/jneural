package sample;

public class Box<T> {
    private T elt;

    public Box(T elt) {
        this.elt = elt;
    }

    public T get(){
        return elt;
    }

    public void set(T elt){
        this.elt = elt;
    }
}
