package sample;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PMatrix {
    public final int numRows;
    public final int numCols;

    private List<List<Double>> matrix;

    public PMatrix(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        matrix = IntStream.range(0, numRows).parallel().mapToObj(j -> Collections.nCopies(numCols, 0D)).collect(Collectors.toList());
    }

    private PMatrix(int numRows, int numCols, List<List<Double>> matrix) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.matrix = matrix;
    }

    public void foreach(Consumer<Double> func) {
        matrix.forEach(row -> row.forEach(func));
    }

    public PMatrix map(Function<Double, Double> func) {
        List<List<Double>> X = matrix.parallelStream()
                .map(row -> row.parallelStream()
                        .map(func).collect(Collectors.toList()))
                .collect(Collectors.toList());
        return new PMatrix(numRows, numCols, X);
    }

    public void setRows(Double[]... rows) {
        for (int i = 0; i < numRows; i++) {
            matrix.set(i, Arrays.asList(rows[i]));
        }
    }


    public Double get(int i, int j) {
        return matrix.get(i).get(j);
    }

    public static PMatrix multiply(PMatrix A, PMatrix B) {
        List<List<Double>> X = IntStream.range(0, A.numRows)
                .parallel()
                .mapToObj(i -> IntStream.range(0, B.numCols)
                        .parallel()
                        .mapToObj(j -> IntStream.range(0, A.numCols)
                                .parallel()
                                .mapToDouble(k -> A.get(i, k) * B.get(k, j)
                                ).sum()
                        )
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        return new PMatrix(A.numRows, B.numCols, X);
    }


    public static PMatrix addColVec(PMatrix A, PMatrix V) {
        List<List<Double>> X = IntStream.range(0, V.numRows)
                .parallel()
                .mapToObj(i ->
                        A.matrix.get(i).parallelStream()
                                .map(vElt -> V.get(i, 0) + vElt)
                                .collect(Collectors.toList()))
                .collect(Collectors.toList());
        return new PMatrix(A.numRows, A.numCols, X);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PMatrix{\n");
        for (int i = 0; i < numRows; i++) {
            sb.append("[");
            for (int j = 0; j < numCols; j++) {
                sb.append(matrix.get(i).get(j));
                if (j != numCols - 1) {
                    sb.append(" ");
                }
            }
            sb.append("]\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
