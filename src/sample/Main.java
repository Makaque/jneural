package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Collections;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
//        Matrix A = new Matrix(2, 3);
//        Matrix B = new Matrix(3, 3);
//
//        A.setRows(new double[]{2, 5, 3}, new double[]{4,2,3});
//        B.setRows(new double[]{2, 5, 3}, new double[]{4,2,3}, new double[]{5,2,3});
//
//        Matrix C = Matrix.multiply(A, B);


        PMatrix A = new PMatrix(2, 3);
        PMatrix B = new PMatrix(3, 3);

        A.setRows(new Double[]{2d, 5d, 3d}, new Double[]{4d,2d,3d});
        B.setRows(new Double[]{2d, 5d, 3d}, new Double[]{4d,2d,3d}, new Double[]{5d,2d,3d});

//        System.out.println(A);
//        System.out.println(B);

        PMatrix C = PMatrix.multiply(A, B);
//        System.out.println(C);

        NeuralNetwork nn = new NeuralNetwork(1000,500,10);
        PMatrix input = new PMatrix(1000, 1);
        Double[][] rows = Collections.nCopies(1000, new Double[]{1d}).toArray(new Double[][]{});
//        input.setRows(new Double[]{1d}, new Double[]{1d}, new Double[]{1d});
//        NeuralNetwork nn = new NeuralNetwork(3,5,10);
//        PMatrix input = new PMatrix(3, 1);
//        input.setRows(new Double[]{1d}, new Double[]{1d}, new Double[]{1d});
        input.setRows(rows);
//        nn.predict(input);
        System.out.println(nn.predict(input));

    }


    public static void main(String[] args) {
        launch(args);
    }
}
