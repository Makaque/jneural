package sample;

import java.util.function.Consumer;
import java.util.function.Function;

public class Matrix {
    public final int numRows;
    public final int numCols;

    private double[][] matrix;

    public Matrix(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;

        matrix = new double[numRows][numCols];
        foreach(System.out::println);
    }

    public void foreach(Consumer<Double> func) {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                func.accept(matrix[i][j]);
            }
        }
    }

    public Matrix map(Function<Double, Double> func) {
        Matrix m = new Matrix(numRows, numCols);
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                m.matrix[i][j] = func.apply(matrix[i][j]);
            }
        }
        return m;
    }

    public void setRows(double[]... rows) {
        for (int i = 0; i < numRows; i++) {
            matrix[i] = rows[i];
        }
    }

    public static Matrix multiply(Matrix A, Matrix B) {
        Matrix C = new Matrix(A.numRows, B.numCols);
        for (int i = 0; i < C.numRows; i++) {
            for (int j = 0; j < C.numCols; j++) {
                for (int k = 0; k < A.numCols; k++) {
                    C.matrix[i][j] += A.matrix[i][k] * B.matrix[k][j];
                }
            }
        }
        System.out.println(C);
        return C;
    }

    public static Matrix addColVec(Matrix A, Matrix V){
        Matrix R = new Matrix(A.numRows, A.numCols);
        for(int i = 0; i < A.numRows; i++){
            for(int j = 0 ; j < A.numCols ; j++){
                R.matrix[i][j] = A.matrix[i][j] + V.matrix[i][0];
            }
        }
        return R;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Matrix{\n");
        for (int i = 0; i < numRows; i++) {
            sb.append("[");
            for (int j = 0; j < numCols; j++) {
                sb.append(matrix[i][j]);
                if (j != numCols - 1) {
                    sb.append(" ");
                }
            }
            sb.append("]\n");
        }
        sb.append("}");
        return sb.toString();
    }
}
